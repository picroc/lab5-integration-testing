# Lab5 -- Integration testing

## BVA

- type: "budget", "luxury", "nonsense"
- plan: "minute", "fixed_price", "nonsense"
- distance: -1, 0, 1, 10
- planned_distance: -1, 0, 1, 10
- time: -1, 0, 1, 10
- planned_time: -1, 0, 1, 10
- inno_discount: "yes", "no", "nonsense"

## Table

| type | plan | distance | planned_distance | time | planned_time | inno_discount | expected | actual |
|------|------|----------|------------------|------|--------------|---------------|----------|--------|
| budget | minute | 1 | 1 | 1 | 1 | yes | 14.62 | {"price":17} |
| budget | minute | 1 | 1 | 1 | 1 | no | 17 | {"price":17} |
| budget | minute | -1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| budget | minute | 1 | -1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| budget | minute | 1 | 1 | -1 | 1 | yes | Invalid Request | Invalid Request |
| budget | minute | 1 | 1 | 1 | -1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | 1 | 1 | 1 | yes | 43.86 | {"price":51} |
| luxury | minute | 1 | 1 | 1 | 1 | no | 51 | {"price":51} |
| luxury | fixed_price | 1 | 1 | 1 | 1 | no | Invalid Request | {"price":12.5} |
| budget | fixed_price | 1 | 1 | 1 | 1 | yes | 14.62 | {"price":12.5} |
| budget | fixed_price | 1 | 1 | 1 | 1 | no | 17 | {"price":12.5} |
| budget | fixed_price | 10 | 1 | 1 | 1 | no | 17 | {"price":16.666666666666668} |
| budget | fixed_price | 1 | 1 | 10 | 1 | no | 170 | {"price":166.66666666666666} |
| budget | fixed_price | 1 | 1 | 1 | 1 | no | 17 | {"price":12.5} |
| luxury | minute | 1 | 10 | 1 | 1 | yes | 43.86 | {"price":51} |
| luxury | minute | 1 | 1 | 1 | 10 | yes | 43.86 | {"price":51} |
| luxury | minute | 10 | 1 | 1 | 1 | yes | 43.86 | {"price":51} |
| luxury | minute | 1 | 1 | 10 | 1 | yes | 438.6 | {"price":510} |
| luxury | minute | 0 | 1 | 1 | 1 | yes | Invalid Request | {"price":51} |
| luxury | minute | 1 | 0 | 1 | 1 | yes | Invalid Request | {"price":51} |
| luxury | minute | 1 | 1 | 0 | 1 | yes | Invalid Request | {"price":0} |
| luxury | minute | 1 | 1 | 1 | 0 | yes | Invalid Request | {"price":51} |
| luxury | minute | 10 | 0 | 1 | 1 | yes | Invalid Request | {"price":51} |
| luxury | minute | 1 | 1 | 10 | 0 | yes | Invalid Request | {"price":510} |
| luxury | minute | 1 | 10 | 1 | 10 | yes | 43.86 | {"price":51} |
| nonsense | minute | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | nonsense | 1 | 1 | 1 | 1 | yes | 43.86 | {"price":51} |
| luxury | minute | 1 | 1 | 1 | 1 | nonsense | Invalid Request | Invalid Request |
| luxury | minute | -1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | -1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | 1 | -1 | 1 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1 | 1 | 1 | -1 | yes | Invalid Request | Invalid Request |

# Bugs

- No discount calculation
- No validation for luxury fixed price plan unavailability
- Strange values for fixed price calculation
- No validation for 0's in any of time/distance fields

